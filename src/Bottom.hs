import Xmobar
import Config
import qualified Music

import System.Environment (getArgs)


main :: IO ()
main = do
  args <- getArgs
  palette >>= configFromArgs . Music.config "spotify" >>= xmobar
