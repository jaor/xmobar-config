module Config (
  Palette(..), baseConfig, palette, (<~>), (>~<),
  mkArgs, defaultHeight, fc, fn, fni)
 where

import System.Environment (lookupEnv)

import Xmobar

defaultHeight :: Int
defaultHeight = 24

data Palette = Palette { pNormal :: String
                       , pLow :: String
                       , pHigh :: String
                       , pDim :: String
                       , pFont :: String
                       , pBorder :: String
                       , pForeground :: String
                       , pBackground :: String
                       , pAlpha :: Int
                       , pIconRoot :: String
                       , pIsLight :: Bool
                       }

fc color thing = "<fc=" ++ color ++ ">" ++ thing ++ "</fc>"
fn n thing = "<fn=" ++ show n ++ ">" ++ thing ++ "</fn>"
fni = fn 6

lightTheme :: IO Bool
lightTheme = fmap (== Just "light") (lookupEnv "JAO_COLOR_SCHEME")

icons k = "/home/jao/.config/xmobar/icons/" ++ k

lightPalette :: Palette
lightPalette = Palette { pNormal = "black"
                       , pLow = "#4d4d4d"
                       , pHigh = "#a0522d"
                       , pDim = "grey60"
                       , pFont = "xft:Source Code Pro Medium-9"
                       , pBorder = "grey80"
                       , pForeground = "#000000"
                       , pBackground = "white"
                       , pAlpha = 0
                       , pIconRoot = icons "light"
                       , pIsLight = True
                       }

zenburnRed = "#CC9393"
-- zenburnBack = "#2B2B2B"
zenburnBack = "#1f1f1f"
zenburnBackLight = "#383838"
zenburnFg = "#989890" -- "#DCDCCC"
zenburnYl = "#F0DFAF"
zenburnGreen = "#7F9F7F"
doomBack = "#22242b"

darkPalette :: Palette
darkPalette = Palette { pNormal = zenburnFg
                      , pLow = "darkseagreen4" -- zenburnGreen
                      , pHigh = zenburnRed
                      , pFont = "xft:DejaVu Sans Mono-9"
--                      , pFont = "xft:Noto Sans Mono Medium-9"
--                      , pFont = "xft:PragmataPro-9"
--                      , pFont = "xft:Source Code Pro Medium-9"
--                      , pFont = "xft:IosevkaCC-9"
                      , pDim = "grey50"
                      -- , pFont = "xft:NotoMono-9,xft:Inconsolata-11"
                      , pBorder = "black" -- zenburnBackLight
                      , pForeground = zenburnFg
                      , pBackground = doomBack -- zenburnBack
                      , pAlpha = 255
                      , pIconRoot = icons "dark"
                      , pIsLight = False
                      }

palette :: IO Palette
palette = do
  light <- lightTheme
  if light then return lightPalette else return darkPalette

baseConfig :: Palette -> Config
baseConfig p = defaultConfig {
  font = pFont p
  , borderColor = pBorder p
  , fgColor = pForeground p
  , bgColor = pBackground p
  , additionalFonts = [ "xft:Symbola-9"
                      , "xft:Symbola-10"
                      , "xft:Symbola-11"
                      , "xft:Symbola-11"
                      , "xft:DejaVu Sans Mono-9"
--                      , "xft:Noto Sans Mono Medium-9"
--                      , "xft:IosevkaCC-9"
                      , "xft:FontAwesome-10"]
  , border = NoBorder
  , alpha = pAlpha p
  , overrideRedirect = True
  , lowerOnStart = True
  , hideOnStart = False
  , allDesktops = True
  , persistent = True
  , sepChar = "|"
  , alignSep = "{}"
  , iconRoot = pIconRoot p
  }

(<~>) :: Palette -> [String] -> [String]
(<~>) p args =
  args ++ [ "--low", pLow p , "--normal", pNormal p , "--high", pHigh p]

(>~<) :: Palette -> [String] -> [String]
(>~<) p args =
  args ++ [ "--low", pHigh p , "--normal", pNormal p , "--high", pLow p]

mkArgs :: Palette -> [String] -> [String] -> [String]
mkArgs p args extra = concat [p <~> args, ["--"], extra]
