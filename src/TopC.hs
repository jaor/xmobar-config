import Xmobar
import Config
import Monitors
import Music (mpris, gpmd, mpdt)

import qualified Data.Char as Char
import qualified Text.Printf as Printf

memoratio = Memory ["-t","<usedratio>%", "-p", "2", "-W", "3"] 20

topProcL p = TopProc (p <~> ["-t"
                            , "<both1> <both2> <both3> <both4>"
                              ++ "  ·  <mboth1> <mboth2> <mboth3> <mboth4>"
                            , "-w", "12", "-L" , "10", "-H", "80"]) 15
diskIOL p = DiskIO [("/", "<totalbipat>"), ("/home", "<totalbipat>")]
                   (diskArgs p) 10

mpd a p t i =
  MPDX [ "-W", "12", "-b", "░", "-f", "▒", "-t", t
       , "--", "-p", p, "-P", fni "\xf144", "-Z", i, "-S", i] 20 a


autoMPD light = AutoMPD [ "-T", "110", "-E", "…", "-W", "10", "-t" ,
                          fc "grey40" (fni "\xf001") ++
                            " <lapsed>/<length> <ppos>/<plength>" --  ++ mpdt light
                        ]

pollMPD port light =
  MPD [ "-T", "120", "-W", "10", "-E", "…", "-t", tm, "--", "-p", port] 20
  where tm = fc "grey40" (fni "\xf001") ++ " <remaining> <length> "
   -- ++ "<ppos>/<plength>"
   -- ++ mpdt light

mprisName = "spotifyd"

musicPipe = PipeReader "/tmp/music" "musicpipe"
musicMPRIS p = mpris p mprisName 180

-- music light = toggleMonitor "/tmp/mpris.st" musicPipe (pollMPD light)
-- music p = toggleMonitor "/tmp/mpris.st" (pollMPD (pIsLight p)) (musicMPRIS p)
-- music port = pollMPD port . pIsLight
music = concatMonitor " "
                      (mpd "mpd" "6600" "<statei> <remaining>" (fni "\xf001"))
                      (mpd "mopidy" "6669" "<statei> <ppos>/<plength> <remaining>"
                       (fni "\xf1bc"))

-- "https://wttr.in?format=" ++ fnn 3 "%c" ++ "+%t+%C+%w++" ++ fnn 1 "%m"
wttrURL = "https://wttr.in?format=" ++ fnn 2 "+%c+" ++ "+%t+%C+" ++ fn 5 "%w"
  where fnn n x = urlEncode ("<fn=" ++ show n ++ ">") ++ x ++ urlEncode "</fn>"
        encode c
          | c == ' ' = "+"
          | Char.isAlphaNum c || c `elem` "-._~" = [c]
          | otherwise = Printf.printf "%%%02X" c
        urlEncode = concatMap encode

config p = (baseConfig p) {
  position = TopSize C 100 (defaultHeight - 1)
  , textOffset = defaultHeight - 8
  , textOffsets = [defaultHeight - 9, defaultHeight - 9,
                   defaultHeight - 6, defaultHeight - 8,
                   defaultHeight - 8, defaultHeight - 8]
  , border = FullB
  , alpha = 255
  , commands = [ Run (topProcL p)
               , Run m
               , Run (thinkTemp p)
               , Run (NamedXPropertyLog "_EMACS_LOG" "elog")
               , Run (cpuBars p)
               , Run memoratio
               , Run (diskU p)
               , Run (diskIOL p)
               , Run brightness'
               , Run (kbd p)
               , Run (iconBatt p)
               , Run (wireless p "wlp1s0")
               , Run (dynNetwork p)
               , Run proton0
               , Run (masterVol p)
               , Run captureVol
               , Run laTime
               , Run localTime
               -- , Run w -- LEGE, LEBL, KCV0
               , Run (ComX "curl" [wttrURL] "" "wttr" 18000)
               ]
  , template = " "
             ++ "|batt0| "
             ++ dimi "\xf26c" ++ " |bright| "
             ++ "<action=`toggle-app.sh nm-applet`> |proton0||wlp1s0wi|</action>"
             ++ " |dynnetwork| "
             ++ "<action=`toggle-app.sh pasystray`>"
             ++ "  |default:Master| " ++ dimi "\xf130" ++ " |default:Capture|"
             ++ "</action>  "
             ++  ma
             -- ++ " |EGPH| "
             ++ " |wttr|"
             ++ " {} "
             ++ fc (pHigh p) "|elog|"
             ++ "|kbd| "
             ++ "|multicpu| "
             ++ "|multicoretemp| "
             ++ " |top| "
             ++ fni "\xf0c9" ++ " |memory| "
             -- ++ dimi "\xf1c0"
             ++ "|diskio| |disku| "
             ++ fni "\xf017" ++ " |datetime| "
             ++ "|laTime| "
  } where dimi = fc (pDim p) . fni
          m = music
          ma = " |" ++ alias m ++ "| "
--          w = (weather' "<skyConditionS> <tempC>° <weather>" "EGPH" p)

main :: IO ()
main = palette >>= configFromArgs . config >>= xmobar
