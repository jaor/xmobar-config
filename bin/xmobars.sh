#!/bin/bash

killall trayer

trayer --margin 2 --distancefrom left \
       --distance 1 --edge top \
       --align left --SetDockType true --SetPartialStrut false \
       --widthtype request \
       --height 21 --heighttype pixel \
       --transparent true\
       --alpha 255 --padding 1 &

killall xmobar-exwm
xmobar-exwm &
