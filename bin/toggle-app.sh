#!/bin/bash

(pidof $1 && killall $1) || ($1 &)
